//------------------------------------------------------------------------------
// main.cpp Objektorienterad programmering i C++
//------------------------------------------------------------------------------

#include "../include/proj.h"
#include <iostream>

int main() {
    std::cout << getAssignmentInfo() << std::endl;
    return 0;
}