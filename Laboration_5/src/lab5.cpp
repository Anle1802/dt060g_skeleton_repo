//------------------------------------------------------------------------------
// Lab5.cpp Objektorienterad programmering i C++
//------------------------------------------------------------------------------

#include "../include/Lab5.h"
#include <iostream>

/**
 * Main program
 */
int main() {
  std::cout << getAssignmentInfo() << std::endl;
  return 0;
}
